package hardyappspop.stylesapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

/**
 * Created by luisangelgarcia on 6/6/15.
 */
public class MainActivity extends AppCompatActivity {
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    // ===========================================================
    // Methods
    // ===========================================================
    public void onClickThemeOne(View view) {
        startActivity(new Intent(this, ThemeOne.class));
    }

    public void onClickThemeTwo(View view) {
        startActivity(new Intent(this, ThemeTwo.class));
    }

    public void onClickThemeThree(View view) {
        startActivity(new Intent(this, ThemeThree.class));
    }

    public void onClickThemeFour(View view) {
        startActivity(new Intent(this, ThemeFour.class));
    }

    public void onClickThemeFive(View view) {
        startActivity(new Intent(this, ThemeFive.class));
    }

    public void onClickThemeSix(View view) {
        startActivity(new Intent(this, ThemeSix.class));
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
